use advent_of_code_2021::read_lines_from_file;
use itertools::Itertools;

fn main() {
    println!("Part 1: {}", part1());
    // println!("Part 2: {}", part2());
}

fn part1() -> usize {
    read_lines_from_file("input/day1.txt").unwrap().count()
    // todo!()
}

fn part2() -> usize {
    todo!()
}
