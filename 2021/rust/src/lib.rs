use std::fs::File;
use std::io::{BufRead, BufReader, Error, Lines};

pub fn read_lines_from_file(file_path: &str) -> Result<Lines<BufReader<File>>, Error> {
    Ok(BufReader::new(File::open(file_path)?).lines())
}
